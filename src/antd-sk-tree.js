
import { SkTreeImpl }  from '../../sk-tree/src/impl/sk-tree-impl.js';

export class AntdSkTree extends SkTreeImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'tree';
    }

}
